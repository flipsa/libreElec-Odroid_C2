#!/bin/bash

# This script downloads a chromebook recovery image and extracts the ARMv7 widevine library
# Source: https://www.kodinerds.net/index.php/Thread/44211-Release-Amazon-Prime-Instant-Video/?postID=439060#post439060

# NEEDS TO RUN AS ROOT

curl -s https://dl.google.com/dl/edgedl/chromeos/recovery/recovery.conf | grep -A 12 'Acer Chromebook R13' | grep -o 'https.*' | xargs curl | zcat > /tmp/image.bin

mkdir /tmp/chromeos

MOUNTCMD=$(fdisk -l /tmp/image.bin 2> /dev/null | grep 'bin3 ' | awk '{print "mount -o loop,ro,offset="$2*512" /tmp/image.bin /tmp/chromeos"}')

echo $MOUNTCMD

$($MOUNTCMD)

cp /tmp/chromeos/opt/google/chrome/libwidevinecdm*.so .

umount /tmp/chromeos

rmdir /tmp/chromeos

#rm /tmp/image.bin
